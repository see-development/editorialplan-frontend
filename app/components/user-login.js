import Component from '@ember/component';

export default Component.extend({
	authManager: Ember.inject.service('authManager'),
	
	actions: {
	    login() {
			
	      const { username, password } = this.getProperties('username', 'password');
	      this.get('authManager').login(username, password).then((result) => {
	      }, (err) => {
	       alert('error -> auth service not available');
	      });
	    }, 
		logout(){
			this.get('authManager').logout();
		}
	  }
});
