import Component from '@ember/component';

export default Component.extend({
	authManager: Ember.inject.service('authManager'),
	
	actions: {
	    register() {
			
	      const { username, password } = this.getProperties('username', 'password');
	      this.get('authManager').register(username, password).then((result) => {
	      }, (err) => {
	       alert('error -> auth service not available');
	      });
	    }
	  }
});
