import Ember from 'ember';

export default Ember.Component.extend({
    init() {
        this._super(...arguments);
        var self = this;
		var market = self.get('appcontext').get('data')["market"];
		console.debug(market.displays);
        self.displays = market.displays;
        Ember.run.schedule('afterRender', this, function () {


        });
    },

    didRender: function () {

    }

});

