import Ember from 'ember';
import Resolver from './resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';

let App;

Ember.MODEL_FACTORY_INJECTIONS = true;

App = Ember.Application.extend({
    modulePrefix: config.modulePrefix,
    podModulePrefix: config.podModulePrefix,
    Resolver
});

Ember.Application.initializer({
    name: 'context-adapter',
    initialize: function (application) {
        console.log('run Ember.Application.initializer');
        application.register('context:current', App.Context, {singleton: true});
    }
})

App.create({
    ready: function () {
        console.log('run App.create.run');
        var application = this;
        application.inject('controller', 'appcontext', 'context:current');
        application.inject('component', 'appcontext', 'context:current');
        application.inject('view', 'appcontext', 'context:current');
		application.inject('model', 'appcontext', 'context:current');
        application.inject('service', 'appcontext', 'context:current');
    }
});



App.Context = Ember.Object.extend({
    isReady: false,
    init: function (application) {
        var self = this;
        Ember.$.ajax({
            url: 'http://bmw-cms.apps.seurl.de/api/editorialplan/market/1',
            success: function (result) {

                if (result instanceof Object) {
                    self.set('data', result);
                } else {
                    self.set('data', JSON.parse(result));
                }

                self.isReady = true;
                //location.hash = "#index";
            },
            context: document.body,
            cache: true,
            async: false
        });
    }
});

loadInitializers(App, config.modulePrefix);

export default App;
