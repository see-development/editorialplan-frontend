import { helper } from '@ember/component/helper';

export function phrase(params/*, hash*/) {
	var phrases = window.App.context.get('data').phrases;
	if(phrases[params[0]]){
		return phrases[params[0]];
	}
	
  return params[1] + ' <span class="small" style="color:white;background:purple;padding: 2px 5px;display:inline; text-transform:none"><i>Phrase "'+params[0]+'" not found!</i></span>';
}

export default helper(phrase);
