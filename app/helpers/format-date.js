import Ember from 'ember';

export function formatDate(params/*, hash*/) {
	if(params[0] == null) return "";
	var date = params[0].split("-");
  return date[2] + "." + date[1] + "." + date[0];
}

export default Ember.Helper.helper(formatDate);
