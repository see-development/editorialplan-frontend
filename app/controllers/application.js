import Ember from 'ember';

export default Ember.Controller.extend({
	userview: false,
	init: function(){
		if(this.appcontext){
			this.appcontext.set('currentPath', this.currentRouteName);
		}

	}
});
