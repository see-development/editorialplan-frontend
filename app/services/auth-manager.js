import Service from '@ember/service';

export default Service.extend({
	accessToken: null,
	whiteList: ['user.login', 'user.register'],
	
	init: function(){
		var self = this;
		if(localStorage.getItem("authtoken")){
			self.set('accessToken', localStorage.getItem("authtoken"));
		}
	},
	
	hasAuth: function(){
		var self = this;
		if(self.appcontext && ($.inArray(self.appcontext.currentPath, self.get('whiteList')) == 0)){
			console.debug('----- AUTH MANAGER: ROUTE IN WHITELIST ----');
			return true;
		}
		console.debug('----- AUTH MANAGER: ROUTE NOT WHITELISTET ----');
		return this.get('accessToken');
	},
	
	register: function(username, password){
       return Ember.$.ajax({
            url: 'http://bmw-cms.apps.seurl.de/api/editorialplan/market/1/user/register',
            type:"post",
			data: {username:username, password:password},
            context: document.body,
            cache: false,
            async: false
        }).then((result) => {
      	  	if(result.success == true){
				$('.error').addClass('hide');
				alert('Welcome, please login now.');
      	  		location.hash = "/user/login";
      	  	}else{
      	  		$('.error').removeClass('hide');
      	  	}
    	});
	},
	
	login: function(username, password){
	var self = this;
       return Ember.$.ajax({
            url: 'http://bmw-cms.apps.seurl.de/api/editorialplan/market/1/user/login',
            type:"post",
			data: {username:username, password:password},
            context: document.body,
            cache: false,
            async: false
        }).then((result) => {
      	  	if(result.success == true){
				$('.error').addClass('hide');
				self.set('accessToken', result.session)
				localStorage.setItem("authtoken", result.session);
				location.hash = "#/";
				location.reload();
      	  	}else{
      	  		$('.error').removeClass('hide');
      	  	}
    	});
	},
	
	logout: function(){
		localStorage.setItem("authtoken", null);
		localStorage.removeItem("authtoken");
		location.href = "#/";
		location.reload();
	}
});
