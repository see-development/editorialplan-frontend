import Ember from 'ember';
import ENV from '../config/environment';

export default Ember.Service.extend({
	

	init: function(){
		console.debug('------- DATA SERVICE INIT ---------');
	},

	loadDisplay: function(displayId, playlistId, spot_id){
		var self = this;
		var data = self.get('appcontext').get('data')['market'];
		var resultDisplay = false;
		var resultPlaylist = false;
		$.each(data.displays, function(index, display){
			if(display.id == displayId){
				resultDisplay = display;
				$.each(display.playlists, function(i, playlist){
					if(playlist.id == playlistId){
						resultPlaylist = playlist;
					}
				});
			}
		});
		resultPlaylist = (resultPlaylist === false)?resultDisplay.playlists[0]: resultPlaylist;
		var resultSpot = false;
		if(resultDisplay.istouchapp){
			if(spot_id){
				$.each(resultPlaylist.spots, function(index, spot){
					if(spot.id == spot_id){
						resultSpot = spot;
					}
				})
			}else{
				resultSpot = resultPlaylist.spots[0];
			}
		}
		return {display:resultDisplay, playlist: resultPlaylist, spot: resultSpot};
		
	},
	loadMarket: function(){
		var self = this;
		return self.get('appcontext').get('data')['market'];
		
	},
	
	loadPage: function(pageKey){
		var self = this;
		var data = self.get('appcontext').get('data')['market'];
		var pageModel = false;
		$.each(data.pages, function(index, page){
			if(page.systemkey == pageKey){
				pageModel = page;
			}
		});
		return pageModel;
	},
	
	urlParam: function (name) {
	    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
	                      .exec(window.location.href);
		if(results == null) return false

	    return results[1] || 0;
	}
});
