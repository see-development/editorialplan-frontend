import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
    this.route('display', { path: '/display/:display_id/playlist/:playlist_id' });
	this.route('spot', { path: '/display/:display_id/playlist/:playlist_id/spot/:spot_id' });
	this.route('page', { path: '/page/:page_key' });
	this.route('user', function() {
	    this.route('login');
		this.route('register');
	  });
	this.route('contacts');
});

export default Router;
