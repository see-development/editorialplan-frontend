export function initialize(application) {
  application.context = application.__container__.lookup('context:current')
  window.App = application;  // or window.Whatever
}

export default {
  name: 'global',
  initialize: initialize
};