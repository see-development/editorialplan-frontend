import Ember from 'ember';
import RouteModelMixin from '../mixins/route-model';



export default Ember.Route.extend(RouteModelMixin,{
	model: function(params) {
	  return this.get('dataService').loadDisplay(params.display_id, params.playlist_id, params.spot_id);
	}
});
