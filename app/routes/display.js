import Ember from 'ember';
import RouteModelMixin from '../mixins/route-model';



function getTime(time) {
  var seconds = parseInt(time.split(":")[1], 10);
  var minutes = parseInt(time.split(":")[0], 10);

  return minutes * 60  + seconds;
}

function convertTime(time) {
    var newMinutes = Math.floor(time/60);
    var newSeconds = time - newMinutes * 60;

  function str_pad_left(string,pad,length) {
    return (new Array(length+1).join(pad)+string).slice(-length);
  }
  return str_pad_left(newMinutes,'0',2)+':'+str_pad_left(newSeconds,'0',2);
}



export default Ember.Route.extend(RouteModelMixin,{
	model: function(params) {
	  var res = this.get('dataService').loadDisplay(params.display_id, params.playlist_id);
      res.display.playlists.forEach(function (currentPlaylist) {
        var totalDuration = 0;

        currentPlaylist.spots.forEach(function (spotItem, index) {
          totalDuration+=getTime(spotItem.duration);

          var nth = 4;
          if (index % nth == 0) {
            Ember.set(spotItem, 'showMenu', true);
          }
        });
        Ember.set(currentPlaylist, 'totalDuration', convertTime(totalDuration));
      });
		return res;
	}
});
