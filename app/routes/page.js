import Ember from 'ember';
import RouteModelMixin from '../mixins/route-model';

export default Ember.Route.extend(RouteModelMixin,{
	model: function(params) {
		return this.get('dataService').loadPage(params.page_key);
	}
});
