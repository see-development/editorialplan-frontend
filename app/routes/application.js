import Ember from 'ember';

export default Ember.Route.extend( {
	authManager:Ember.inject.service('authManager'),

	init() {
		var self = this;
		//self.get('controller').appcontext.set('currentPath', this.get('controller').currentRouteName);
		if(!self.get('authManager').hasAuth()){
			//alert('no auth found, please login or register');
			location.hash="/user/login";
		}
        },


  actions: {
    didTransition: function () {
		if(this.get('controller').appcontext){
			console.debug('----- APP ROUTER: WRITE CURRENT ROUTE TO CONTEXT----');
			this.get('controller').appcontext.set('currentPath', this.get('controller').currentRouteName);
		}
		
		return true;
    },
	willTransition: function () {
		var self = this;
		if(!self.get('authManager').hasAuth()){
			//alert('no auth found, please login or register');
			location.hash="/user/login";
		}
		return true;
	},
	logout: function(){
		var self = this;
		self.get('authManager').logout();
	}
  }
});
