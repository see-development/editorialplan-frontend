import Ember from 'ember';

export default Ember.Mixin.create({
  dataService: Ember.inject.service('data'),
  beforeModel() {
	  this._super();
  },

  model() {
    return this.get('dataService').getData();
  }
});
